<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/kronz/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#F0F0F0;">
            <div class="work_semi_circle">
                <div class="semi_circle" style="transform:rotate(0deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" preload="/assets/works/kronz/imgs/lowRes/logo.png" src="/assets/works/kronz/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Kronz<br>store.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Client</div>
                        <div>Moi même</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Dates</div>
                        <div>Dec 2018 - Fev 2019</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Localisation</div>
                        <div>Domicile</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Work</div>
                        <div>Strategy, UX research,<br> concepts, messaging hierarchy, <br>web design (IA, IxD, design, UX/UI design, prototyping, animation), <br>TOV, Back developper, content definition.</div>
                    </div>
                </div>
                <div class="right_cont"><div>Kronz store est un site de dropshipping totalement opérationnel et spécialisé dans la vente de montres. Réalisé en un peu plus d'un mois, il met en application les technologies Javascript, du Php, et des APIs (comme Stripe).</div>
                <div class="link"><a class=" hover-target" href="/assets/works/kronz/files/competences.png" target="_blank">Voir les compétences.</a></div>
                <div class="link"><a class=" hover-target" href="//kronz.franckdsf.com" target="_blank">Voir le site.</a></div>
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div class="cont"><div style="width:84%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img1.png"  src="/assets/works/kronz/imgs/img1.png"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">01.</div>
                    <div><div class="title">Le site.</div>
                    <div class="description">Quelques images du site.</div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img2.png" src="/assets/works/kronz/imgs/img2.png"></div></div>
            <div class="cont">
                <div style="width:25%; margin-left:8%;"><div data-pinned class="descr_cont">
                    <div class="title">La page watches.</div>
                    <div class="description">C'est la page principale du site regroupant tous les articles à disposition des clients.</div>
                </div></div>
                <div style="width:60%; margin-left:5%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img3.png" src="/assets/works/kronz/imgs/img3.png"></div>
            </div>
            <div class="cont"><div style="width:100%; margin-left:0%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img4.png" src="/assets/works/kronz/imgs/img4.png"></div></div>
            <div class="cont">
                <div style="width:20em; margin-left:65%; text-align:right;"><div class="descr_cont">
                    <div class="title">La page produit.</div>
                    <div class="description">Elle est générée automatiquement avec les caractéristiques du produit stockées dans la base de données.</div>
                </div></div>
            </div>
            <div data-bk="rgb(32, 32, 32)" data-color="white"></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">02.</div>
                    <div><div class="title">L'achat.</div>
                    <div class="description">Quelques images de la section achat.</div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img5.png" src="/assets/works/kronz/imgs/img5.png"></div></div>
            <div class="cont">
                <div style="width:25em; margin-left:8%; text-align:left;"><div class="descr_cont">
                    <div class="title">Le panier.</div>
                    <div class="description">Il permet au client de gérer ses articles, leurs quantitées, ainsi que d'ajouter un code promotionnel avant le paiement.</div>
                </div></div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:15%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img6.png" src="/assets/works/kronz/imgs/img6.png"></div></div>
            <div class="cont"><div style="width:80%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img7.png" src="/assets/works/kronz/imgs/img7.png"></div></div>
            <div class="cont mobile_column_cont_1">
                <div style="width:20%; margin-left:20%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img10.png" src="/assets/works/kronz/imgs/img10.png"></div>
                <div style="width:25%; margin-left:5%;"><div data-pinned class="descr_cont">
                    <div class="title">Le mail d'achat.</div>
                    <div class="description">Un mail de confirmation d'achat est envoyé au client lors de la confirmation du paiement. Celui-ci comporte les informations essentielles de l'achat ainsi qu'un lien pour suivre les colis.</div>
                </div></div>
            </div>
            <div data-bk="rgba(247, 247, 247,1)" data-color="black"></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">03.</div>
                    <div><div class="title">Le panel administrateur.</div>
                    <div class="description">Ce panel permet de valider des achats, d'ajouter les liens pour suivre les colis, ect.</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:35%; margin-left:10%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img8.png" src="/assets/works/kronz/imgs/img8.png"></div>
                <div data-smooth-from="5em" data-smooth-to="0" style="width:45%; margin-left:4%; margin-top:-10%;"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img9.png" src="/assets/works/kronz/imgs/img9.png"></div>            
            </div>
            <div class="cont"><div style="width:90%; margin-left:5%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img11.png" src="/assets/works/kronz/imgs/img11.png"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">04.</div>
                    <div><div class="title">La base de données.</div>
                    <div class="description"></div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/kronz/imgs/lowRes/img12.png" src="/assets/works/kronz/imgs/img12.png"></div></div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="axopen">Axopen.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>