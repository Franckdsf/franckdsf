<html xmlns:og="http://ogp.me/ns#">
<head>
<link href="https://fonts.googleapis.com/css?family=Cinzel:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/assets/css/app.css">
<link rel="icon" type="image/png" href="/assets/imgs/flavicon.png">
<script
  src="//code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
  crossorigin="anonymous"></script>  

<!-- Open Graph Protocol (OGP) -->
<meta property="og:image" content="/assets/imgs/opg-img.jpg" />
<meta property="og:description" content="Developpeur, UX & UI designer" />
<meta property="og:url"content="//www.franckdsf.com" />
<meta property="og:title" content="Portfolio" />     
<!---->

<!-- scrollmagic -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.14.2/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/animation.gsap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/debug.addIndicators.js"></script>

<!-- typed text effect - page about / cover -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.10/typed.min.js"></script>
<!-- cookie plugin -->
<script src="/assets/js/jquery.cookie.js"></script>
<!-- create options parameter functions - page transition -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
<!-- Hover Attract plugin -->
<script src="/assets/js/hoverAttract.js"></script>
<!-- anim text - page index -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<!-- smooth scrollbar -->
<script src="/assets/js/smooth-scrollbar.js"></script>
<!-- font awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- autosize textarea - page contact -->
<script src="//cdn.jsdelivr.net/npm/autosize@4.0.2/dist/autosize.min.js"></script>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">

<title>Franck Desfrançais - Portfolio</title>
</head>

<?php require($_SERVER['DOCUMENT_ROOT']."/views/partials/menu.php"); ?>
<?php require($_SERVER['DOCUMENT_ROOT']."/views/partials/transitions.php"); ?>
<?php require($_SERVER['DOCUMENT_ROOT']."/views/partials/first_visit.php"); ?>
<body>
    <div class='cursor' id="cursor"><div class="loading"></div><div class="normal"></div><div class="hover"><div class="text">VIEW</div></div></div>
    <div id="app-content">
        <div id="page-index" page-name="index">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/index.php"); ?>
        </div>
        <div id="page-works" page-name="works">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/works.php"); ?>
        </div>
        <div id="page-about" page-name="about">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/about.php"); ?>
        </div>
        <div id="page-contact" page-name="contact">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/contact.php"); ?>
        </div>
        <div id="page-work" page-name="work">
            <?php 
                if(isset($work)){
                    include($work); 
                }
            ?>
        </div>
    </div>
</body>

<!--force https-->
<script>
    if (location.protocol != 'https:')
    {
        location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    }
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $("#cursor").hide();
        $("body").css("cursor","initial");
    }
</script>
<script>
//global variables
var global_previous_page = {div:"page-index",page_name:"index",url:"/"};
var global_current_page = {div:"page-index",page_name:"index",url:"/"};
//global pages
var global_index_page = {div:"page-index",page_name:"index",url:"/"};
var global_works_page = {div:"page-works",page_name:"works",url:"/works"};
var global_about_page = {div:"page-about",page_name:"about",url:"/about"};
var global_contact_page = {div:"page-contact",page_name:"contact",url:"/contact"};

function assignPages(prev_page, cur_page){
    global_previous_page = prev_page;
    global_current_page = cur_page;

    link = cur_page.url;
    window.history.pushState("", "", window.location.origin+link);
    InitPages();
}
function InitPages(){
    //Make menu button goes back to index
    //On works page
    if(global_current_page.page_name == "works"){
        global_previous_page = global_index_page;
    }
    //Make menu button goes back to works
    //On index page
    if(global_current_page.page_name == "index"){
        global_previous_page = global_works_page;
    }
}

// cursor
function refreshHoverTargets(){
    var e = document.getElementById("cursor");
    function n(t) {
        var text = null;
        for (let element of t.path) {
            if($(element).attr('text-hover') != null){
                text = $(element).attr('text-hover');
                e.classList.add("hover-text");
                $(e).find(".text").html(text);
            }
        }
        if(text == null){
            e.classList.add("hover");
        }
    }
    function s(t) {
        e.classList.remove("hover");
        e.classList.remove("hover-text");
    }
    s();
    for (var r = document.querySelectorAll(".hover-target"), a = r.length - 1; a >= 0; a--) {
        o(r[a])
    }
    function o(t) {
        t.addEventListener("mouseover", n), t.addEventListener("mouseout", s)
    }
}
$(document).ready(function($) { "use strict";
    refreshHoverTargets();
    $(document).on('mousemove', function(e){
        $('#cursor').css({
        left:  e.pageX,
        top:   e.pageY
        });
    });
}); 
//Previous and Next Page trigger
window.addEventListener('popstate', function(event) {
    // The popstate event is fired each time when the current history entry changes.
    window.location = window.location.href;
}, false);
</script>

<script type="text/javascript">
    $(document).ready(function($) {
        var page = <?php echo '"'.$page.'"';?>;
        var cur_page = {div:"#page-index",page_name:page,url:window.location.pathname};

        $("#app-content").children().each(function(){
            $(this).css('display',"none");
            if($(this).attr('page-name') == page){
                $(this).show();
                cur_page.div = $(this).attr('id');
            }
        });

        assignPages(global_previous_page, cur_page);
    });
</script>