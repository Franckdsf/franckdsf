<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/realite-virtuelle/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#877555">
            <div class="work_semi_circle">
                <div class="semi_circle" style="transform:rotate(-90deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="#FFEECA" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="#FFEECA" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" src="/assets/works/realite-virtuelle/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Réalité<br>Virtuelle</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Type</div>
                        <div>Veille technologique</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Durée</div>
                        <div>Novembre 2018 - En cours</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Travail</div>
                        <div>Suivi des actualités concernant<br> la réalité virtuelle.</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Sources</div>
                        <div><a class="hover-target" target="_blank" href="//www.realite-virtuelle.com/">Realite-virtuelle.com</a></div>
                        <div><a class="hover-target" target="_blank" href="//www.lesnumeriques.com/">Les numériques</a></div>
                        <div><a class="hover-target" target="_blank" href="//www.usine-digitale.fr">Usine digitale</a></div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Documentation</div>
                        <div><a class="hover-target" target="_blank" href="/assets/works/realite-virtuelle/files/veilletechnologique.pptx">Powerpoint de présentation</a></div>
                    </div>
                </div>
                <div class="right_cont">La réalité virtuelle est l’ensemble des technologies permettant d’immerger complètement l’utilisateur dans un monde fictif (film, jeu vidéo, univers 3D virtuel…). Les périphériques concernés ont tous en commun de se présenter sous la forme de casque occultant : l’idée est de couvrir tout le champ du regard avec un affichage panoramique au plus près des yeux.
Avec les progrès de la technologie, il est également possible de déplacer sa tête et son regard à 360° dans un monde virtuel, d’effectuer certains gestes par la reconnaissance de mouvements, et parfois, de se déplacer sur quelques mètres autour de soi, tant dans la réalité que dans l’univers parallèle proposé par le casque.</div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div class="cont"><div style="width:84%; margin-left:8%"><img width="100%" height="auto" src="/assets/works/realite-virtuelle/imgs/img6.png"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:37em;">
                    <div class="number">01.</div>
                    <div><div class="title">A quoi ça sert ?</div>
                    <div class="description"></div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:10%;"><div class="descr_cont">
                    <div class="description">Potentiellement, à tout comme à rien. La réalité virtuelle offre avant tout un niveau d’immersion inédit dans un jeu vidéo, puisque le joueur ne se trouve plus face au jeu, mais entouré par celui-ci. Elle permet ainsi de se projeter entièrement dans des mondes imaginaires, univers peuplé de dragons ou espace intersidéral, ou de parcourir les cieux à la manière d’un oiseau. Autant de « voyages » inaccessibles au commun des mortels.
                        <br><br>Mais elle est également un outil professionnel très pratique pour la formation, que ce soit pour l’entraînement au vol ou à la chirurgie, domaines dans lesquels elle offre une mise en situation poussée, tout en minimisant les dangers liés à l’échec. Monde fictif oblige, personne ne vous en voudra si vous sectionnez malencontreusement l’aorte lors de l’opération de l’appendice de votre cobaye virtuel.
                        <br><br>Plus généralement, la réalité virtuelle offre de nombreuses possibilités pour le divertissement : films à 360° en « cinémasphère », clips musicaux englobants, visites virtuelles de musées… sont quelques-unes des mille et une applications imaginables.</div>
                </div></div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:10%;"><div class="descr_cont">
                    <div class="title">On confond souvent réalité augmentée et réalité virtuelle.  Quelle est la différence entre réalité virtuelle et réalité augmentée ?</div>
                    <div class="description">
                        La réalité virtuelle se vit à travers un casque tel que le HTC Vive ou l’Oculus Rift. Le but ici est d’être immergé dans un décor 100% créé pour l’occasion, qui ne prend pas comme base une chambre ou un salon. Le lieu dans lequel on se trouve importe peu car l’entier du décor est recréé à travers le casque. Imaginez-vous en voyage sur Mars.
                        <br><br>
                        La réalité augmentée prend comme base la réalité et y ajoute des couches virtuelles qui sont des hologrammes, que vous pouvez voir à travers des lunettes ou un casque.Ici, la notion de couche virtuelle est importante car sur votre table (qui est réelle), vous allez faire apparaître un hologramme représentant le design d’une moto par exemple, ou vous allez avoir sur votre mur un écran vous affichant Netflix. Le casque Hololens de Microsoft en est un exemple (même s'il se rapproche plus de la mixed reality).
                    </div></div></div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">02.</div>
                    <div><div class="title">Avant.</div>
                    <div class="description">À quoi ressemblait la réalité virtuelle auparavant ?</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:50%; margin-left:10%"><img width="100%" height="auto" src="/assets/works/realite-virtuelle/imgs/img3.jpg"></div>
                <div data-smooth-from="5em" data-smooth-to="0" style="width:30%; margin-left:4%; margin-top:-10%;"><img width="100%" height="auto" src="/assets/works/realite-virtuelle/imgs/img2.jpg"></div>            
            </div>
            <div class="cont">
                <div style="width:25%; margin-left:8%;"><div data-pinned class="descr_cont">
                    <div class="title">Eric M. Howlett.</div>
                    <div class="description">Il fut l’un des premiers a théorisé concrètement la manière de rendre une image virtuelle. Son schéma fut rendu célèbre parmi les développeurs de jeu et de support. Il est à la base de toutes les machines actuelles. C'est la Nasa qui rachètera les brevets. Un casque fut produit en 1979 mais vendu à plus de 10 000 €, peu de personnes achetèrent l’objet.</div>
                </div></div>
                <div style="width:60%; margin-left:5%">
                    <img width="100%" height="auto" src="/assets/works/realite-virtuelle/imgs/img4.jpg">
                    <img width="100%" style="margin-top:5em"height="auto" src="/assets/works/realite-virtuelle/imgs/img5.png">    
                </div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">03.</div>
                    <div><div class="title">Maintenant.</div>
                    <div class="description">Comment est la réalité virtuelle actuellement ?</div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" src="/assets/works/realite-virtuelle/imgs/img7.jpg"></div></div>
            <div class="cont"><div data-smooth-from="30px" data-smooth-to="-50px" style="width:80%; margin-left:8%"><img width="100%" height="auto" src="/assets/works/realite-virtuelle/imgs/img8.jpg"></div></div>
            <div class="cont">
                <div style="width:70%; margin-left:10%;"><div class="descr_cont">
                    <div class="title">Qu'est ce que la mixed reality (realité mixte) ?</div>
                    <div class="description">
                    Le terme réalité mixte est apparu assez récemment. Ce terme hybride permet de classifier une technologie se trouvant entre la frontière mouvante et imprécise de la réalité virtuelle et celle de la réalité mixte. 
                    La réalité mixte est caractérisée par un mélange entre un casque de réalité virtuelle et des caméras de realité augmentée. C'est la technologie du futur qui va se développer et permettre d'avoir en temps réel des indications sur des éléments de l'environnement.
                    Comme dans le domaine militaire ou l'Hololens s'est vendu en masse pour permettre aux combattants sur le terrain d'avoir une vision des autres membres de l'équipe.</div></div></div>
            </div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="axopen">Axopen.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>