<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/notes-app/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#FF8734;">
            <div class="work_semi_circle" style="display:none">
                <div class="semi_circle" style="transform:rotate(90deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" height="auto" preload="/assets/works/notes-app/imgs/lowRes/logo.png" src="/assets/works/notes-app/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Notes<br>App.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">But</div>
                        <div>Travail Personnel pour se perfectionner sur les frameworks.</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Travail</div>
                        <div>Strategy, UX research, concepts, web design (IA, IxD, design, UX/UI design, prototyping, animation).</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Technologies</div>
                        <div>
                            Electron JS, Php, Jquery, Ajax, JSON, MySQL & VueJS.
                        </div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Documentations</div>
                        <div>
                        <a class="hover-target" target="_blank" href="/assets/works/notes-app/files/Notes_Client_Lourd-Documentation_Technique.docx">Documentation Technique Client Lourd</a><br>
                        </div>
                        <div>
                        <a class="hover-target" target="_blank" href="/assets/works/notes-app/files/Notes_Web-Documentation_Technique.docx">Documentation Technique Client Web</a><br>
                        </div>                        
                        <div>
                        <a class="hover-target" target="_blank" href="/assets/works/notes-app/files/Notes-Documentation_Utilisateur.docx">Documentation Utilisateur</a><br>
                        </div>
                    </div>
                </div>
                <div class="right_cont"><div>Notes app est une application bloc note sur le cloud. Il existe une version client lourd (Electron JS) et deux versions web. Dont une en VueJS.</div>
                <div><br>
                    <b style="font-weight:400">Identifiant :</b> test<br>
                    <b style="font-weight:400">Mot de passe :</b> test
                </div><br>
                <div class="link"><a class=" hover-target" href="http://vue-enote.hol.es" target="_blank">Voir l'application en VueJS.</a></div>
                <div class="link"><a class=" hover-target" href="https://mega.nz/#!EpVHzQ7C!4-9QmLLUiQxKLq6C9zS_l6BBJGdvfb6rp5yFTta8DDY" target="_blank">Télécharger l'application.</a></div>
                <div class="link"><a class=" hover-target" href="http://eaelectron.hol.es" target="_blank">Voir l'application web.</a></div>
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">01.</div>
                    <div><div class="title">Présentation.</div>
                    <div class="description">Pourquoi cette application.</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:12%;"><div class="descr_cont">
                    <div class="description">L'application Notes app est une application de notes dans le cloud. Il existe aujourd'hui 3 applications :
                        <br>- une client lourd : <b style="font-weight:400">ElectronJS</b>
                        <br>- une web : <b style="font-weight:400">Php</b>
                        <br>- une web : <b style="font-weight:400">VueJS</b>
                    <br><br>Elle possède une API ouverte permettant de developper l'application avec differents langages et pour différentes plateformes. 
                    </div>
                </div></div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:calc(92% - 15em); width:15em; text-align:right;">
                    <div class="number">02.</div>
                    <div><div class="title">L'Api.</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:20%; text-align:right;"><div class="descr_cont">
                    <div class="description">L'API est stockée sur le serveur de l'application web PHP. Elle permet de récuperer les notes, en créer, les éditer et les supprimer. Du côté des comptes, Elle permet de créer des comptes et de se connecter.
                    <br><br> L'API est codée en PHP et permet via une requête GET ou POST de contacter la base de données.
                    </div>
                </div></div>
            </div>
            <div data-bk="rgb(22, 22, 22)" data-color="white"></div>
            <div class="cont"><div style="width:84%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/notes-app/imgs/lowRes/img1.png"  src="/assets/works/notes-app/imgs/img1.png"></div></div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/notes-app/imgs/lowRes/img2.png" src="/assets/works/notes-app/imgs/img2.png"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">03.</div>
                    <div><div class="title">Notes.</div>
                    <div class="description">Page d'édition et de création de notes.</div>
                    </div>
                </div>
            </div>
            <div data-bk="rgba(247, 247, 247, 1)" data-color="black"></div>
            <div class="cont"><div style="width:84%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/notes-app/imgs/lowRes/img3.png" src="/assets/works/notes-app/imgs/img3.png"></div></div>
            <div class="cont mobile_column_cont_1">
                <div style="width:25%; margin-left:8%;"><div data-pinned class="descr_cont">
                    <div class="title">Page création de note.</div>
                    <div class="description">C'est la même que la page d'édition. Elle permet d'ajouter une note grâce à Quill. Une fois le formulaire rempli, il est envoyé en JSON à l'API qui se charge de reconvertir les données en Array, de les passer en parametres dans une requete SQL afin de les inserer dans la base de données.</div>
                </div></div>
                <div style="width:60%; margin-left:5%">
                    <img width="100%" height="auto" preload="/assets/works/notes-app/imgs/lowRes/img4.png" src="/assets/works/notes-app/imgs/img4.png">
                </div>
            </div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="kronz">Kronz store.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>