<head>
<link rel="stylesheet" type="text/css" href="/assets/css/about.css">
</head>

<body>
<div class="parent_page_cont_about">
	<div class="about-cont">
	<div class="sub-cont-about" id="aboutcont_ID_ieos" data-tilt>
		<div class="age">20 ans</div>
		<div class="name">Franck Desfrançais</div>

		<div class="pre-text">
			Je suis étudiant a l'institution des Chartreux / CPE à Lyon en ingénieur en cybersécurité. Passionné d'informatique et de nouvelles technologies, j'ai déjà développé dans un cadre personnel et autodidacte. J'ai utilisé la plateforme Unity afin de réaliser mes projets en codant grâce au langage objet C#.
			<br><br>Depuis 3 ans, je développe des sites web grâce au HTML, PHP, CSS, JavaScript, Jquery, Ajax, ScrollMagic, Java Web (JEE) et le framework Laravel. Plus récemment je m'interesse aux frameworks Javascript en developpant des applications avec les framework ElectronJS, Angular ou encore VueJS.
		</div>

		<div class="reseaux">
			Liens
			<div class="icons-cont">
			    <a class="hover-target" href="/assets/files/Franck-Desfrancais-CV.pdf" target="_blank">Curriculum vitae.</a>
				<a class="hover-target" href="mailto:franck.desfrancais@gmail.com">Mail.</a>
				<!-- <a class="hover-target" href="https://www.facebook.com/franck.dsf" target="_blank">Facebook.</a> -->
				<!-- <a class="hover-target" href="https://www.instagram.com/franck.dsf" target="_blank">Instagram.</a> -->
			</div>
		</div>
	</div>
	</div>
</div>
</body>

<script>
$(document).ready(function(){
  var description_about_page = $("#aboutcont_ID_ieos > .pre-text").html();
  var descriptionCont_about_page = "<div class='text'>"+"</div>";
  $(descriptionCont_about_page).appendTo($("#aboutcont_ID_ieos > .pre-text"));

  var typed = new Typed('#aboutcont_ID_ieos > .pre-text > .text', {
    strings: [description_about_page],
    typeSpeed: 50,
    loop:true,
    backDelay:5000
  });
});
</script>

<!-- Scroll Magic -->
<script>
$(document).ready(function(){
	return;
  	var controller = new ScrollMagic.Controller();
    var tween = TweenMax.fromTo($("#aboutcont_ID_ieos").children(".name"), 1, {top: "0px"}, {top: "-50px"});
    var scene = new ScrollMagic.Scene({triggerElement:"#aboutcont_ID_ieos", duration:"500", offset:"400"}).setTween(tween).addIndicators().addTo(controller);
});
</script>

<script>
$(document).ready(function(){
    return;
	VanillaTilt.init(document.querySelector("#aboutcont_ID_ieos"), {
		max: 25,
		speed: 400,
		reverse: false
	});
});
</script>