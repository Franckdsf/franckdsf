<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/index_welcome.css">
</head>

<body>
    <div class="index_welcome_full_parent hover-target" text-hover="SCROLL">
        <div id="index_bk_letter_apk1_910">
            <div class="LIW_MD_WELCOME" id="letter_K_LIW_MD">K <div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_D_LIW_MD">D <div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_R_LIW_MD">R <div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_L_LIW_MD">L <div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_F_LIW_MD">F <div class="l"></div><div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_A_LIW_MD">A</div>
            <div class="LIW_MD_WELCOME" id="letter_J_LIW_MD">J</div>
            <div class="LIW_MD_WELCOME" id="letter_S_LIW_MD">S</div>
            <div class="LIW_MD_WELCOME" id="letter_O_LIW_MD">O</div>
        </div>
        <div id="index_bk_letter_apk2_910">
            <div class="LIW_MD_WELCOME" id="letter_T_LIW_MD">T <div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_H_LIW_MD">H <div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_E_LIW_MD">E <div class="l"></div><div class="l"></div><div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_F2_LIW_MD">F <div class="l"></div><div class="l"></div></div>
            <div class="LIW_MD_WELCOME" id="letter_G_LIW_MD">G</div>
            <div class="LIW_MD_WELCOME" id="letter_N_LIW_MD">N</div>
            <div class="LIW_MD_WELCOME" id="letter_C_LIW_MD">C</div>
            <div class="LIW_MD_WELCOME" id="letter_V_LIW_MD">V</div>
        </div>
        <div class="index_center_text">
            <div class="ml13_gei_index">
                <div class="text-wrapper" style="padding-top: 0.1em;">
                    <div class="letters IDK_1"></div>
                    <div class="letters IDK_1" id="IDK_2_1">Bienvenue</div>
                    <div class="letters" id="IDK_2_2">Welcome</div>
                    <div class="letters" id="IDK_2_3">ようこそ</div>
                </div>
                <div class="text-wrapper" style="padding: 0 0.18em">
                    <div class="letters IDK_1"></div>
                </div>
            </div>
        </div>
        <div class="fleche_bas_cont_welcome">
            <div class="fleche_bas_welcome"><img src="/assets/imgs/fleche_bas.png"/></div>
        </div>
    </div>
</body>

<script>
// text effect
$(document).ready(function(){
    // Wrap every letter in a span
    $(".index_center_text").css('display','block');
    $('.ml13_gei_index .letters').each(function(){
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
    });

    anime.timeline({loop: false})
    .add({
        targets: '.ml13_gei_index > .text-wrapper > .IDK_1 > .letter',
        translateY: ["1.1em", 0],
        translateX: ["0.55em", 0],
        opacity: [0,1],
        translateZ: 0,
        rotateZ: [180, 0],
        duration: 750,
        easing: "easeOutExpo",
        delay: function(el, i) {
        return 50 * i;
        }
    });

    loopThis();
    function loopThis(){
        //Init
        $("#IDK_2_1").show();
        $("#IDK_2_2").hide();
        $("#IDK_2_3").hide();
        setTimeout(() => {
            loopThis();
            anime.timeline({loop: false}).add({
                targets: '#IDK_2_1 > .letter',
                translateY: [-100,0],
                easing: "easeOutExpo",
                duration: 1400,
                delay: function(el, i) {
                return 30 * i;
                }
            }); 
        }, 9500);

        setTimeout(() => {
            anime.timeline({loop: false}).add({
            targets: '#IDK_2_1 > .letter',
            translateY: [0, "-1.1em"],
            duration: 300,
            easing: "easeInExpo",
            delay: function(el, i) {
            return 50 * i;
            }
            }); 
            setTimeout(() => {
                $("#IDK_2_1").hide();
                $("#IDK_2_2").css('opacity',0);
                $("#IDK_2_2").show();
                anime.timeline({loop: false})
                .add({
                    targets: '#IDK_2_2',
                    opacity: [0,1],
                    scale: [0.2,1],
                    duration: 800,
                    delay: 0
                }).add({
                    targets: '#IDK_2_2',
                    opacity: 0,
                    scale: 1.5,
                    duration: 600,
                    easing: "easeInExpo",
                    delay: 1500
                });

                setTimeout(() => {
                    $("#IDK_2_2").hide();
                    $("#IDK_2_3 > .letter").css({'transform':'translateY(1.2em)',"opacity":"1"});
                    $("#IDK_2_3").show();
                    anime.timeline({loop: false}).add({
                        targets: '#IDK_2_3 > .letter',
                        translateY: ["1.2em", 0],
                        translateZ: 0,
                        duration: 750,
                        delay: function(el, i) {
                        return 100 * i;
                        }
                    }).add({
                        targets: '#IDK_2_3 > .letter',
                        opacity: [1,0],
                        scale: [1,0.2],
                        duration: 1300,
                        delay: 1000
                    }); 
                }, 3100);
            }, 1300);
        }, 2500);
    }
});
// background
$(document).ready(function(){
    fct();

    function fct(){
        rdmanim("#index_bk_letter_apk1_910");
        setTimeout(() => {
            $("#index_bk_letter_apk1_910").animate({opacity:0},1500,function(){
                $("#index_bk_letter_apk1_910").css({"display":"none"});      
            });      
            setTimeout(() => {
                rdmanim("#index_bk_letter_apk2_910");
                setTimeout(() => {
                    $("#index_bk_letter_apk2_910").animate({opacity:0},1500,function(){
                        $("#index_bk_letter_apk2_910").css({"display":"none"});   
                    });     
                    setTimeout(() => {
                        fct();
                    }, 2000);
                }, 10000);
            }, 2000);
        }, 10000);
    }

    function rdmanim(div){
        $(div).animate({opacity:1},1500);
        $(div).css({"display":"block"});    
        $(div).children().each(function(){
            var num = Math.floor(Math.random() * (10 + 1)) -5;
            num = num/10;
            
            $(this).css({"margin-top":num+"em"});
            $(this).animate({"margin-top":"0"},800);

        });
    }
});
</script>