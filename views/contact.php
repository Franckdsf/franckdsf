<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/contact.css">
</head>

<body>
  <div class="full_contact_100_100" id="fc_100">
    <div class="flex">
      <div class="left_cont">
        <div class="img">
          <img src="https://www.upandgo.co.uk/wp-content/themes/upandgo_1-0/images/navigation/contact-animated.gif" width="100%" height="auto">
        </div>
      </div>
      <div class="right_cont">
        <div class="subtitle">
          Vous aimez ce que vous voyez? Vous avez un projet dont vous souhaitez parler? <br>Voulez-vous me poser une question personnelle?
        </div>
        <div class="title">
          <div>Rester</div>
          <div>en contact.</div>
        </div>
        <div class="form" id="formAKOPG5">
          <div class="input_cont">
            <div class="placeholder">Votre nom *</div>
            <input type="text" class="input"  id="name_la5">
            <div class="bar"></div>
          </div>
          <div class="input_cont">
            <div class="placeholder">Votre e-mail  </div>
            <input type="text" class="input">
            <div class="bar"></div>
          </div>
          <div class="input_cont">
            <div class="placeholder">Votre message *</div>
            <textarea class="input" id="txtarea_la5" rows="1"></textarea>
            <div class="bar"></div>
          </div>
          <div class="send hover-target" id="sendMailButton_1036"><a>Envoyer.</a></div>
        </div>
      </div>
    </div>
    <div class="add_infos">
      <div class="sub">Ou par mail</div>
      <div class="text"><a class="hover-target" href="mailto:franck.desfrancais@gmail.com" id="mailAdress91039">franck.desfrancais@gmail.com</a></div>
    </div>
    <div class="add_infos">
      <div class="sub">Me suivre</div>
      <div class="flex">
        <div class="text"><a class="hover-target">Facebook.</a></div>
        <div class="text"><a class="hover-target">Instagram.</a></div>
        <div class="text"><a class="hover-target">Snapchat.</a></div>
      </div>
    </div>
    <div class="contact_bottom_page">     
        <div class="txt_btm_right_GQO92">
            <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
            <div>·</div>    
            <div>© 2019 Franck Desfrançais.</div>
        </div>      
    </div>
  </div>
</body>

<script>
$(document).ready(function(){
   var scrollbar = Scrollbar.init(document.getElementById('fc_100'), { speed: 0.75 });
   
  autosize($('#txtarea_la5'));

  $("#formAKOPG5").find(".input_cont").each(function(){
    $(this).focusin(function(){
      $(this).addClass("active");
    });
    $(this).focusout(function(){
      if($(this).find('.input').val() == ''){
        $(this).removeClass("active");
      }
    });
  });

  //mail function
  $("#sendMailButton_1036").click(function(){
    let subject = $("#name_la5").val();
    let body = encodeURIComponent($.trim($("#txtarea_la5").val()));
    let mail = $("#mailAdress91039").html();
    let href = $("#sendMailButton_1036").find("a");

    $(href).attr("href", "mailto:"+mail+"?subject=[CONTACT] "+subject+"&body="+body);
  });
 
});
</script>